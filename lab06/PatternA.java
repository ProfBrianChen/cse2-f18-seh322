////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Pattern A
//Lab 06
import java.util.Scanner;
public class PatternA{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //must import scanner
    
    boolean flag = true;
    int numOfRows = 0;
    while (flag){
      System.out.println("Enter a value from 1-10:");
      boolean right = myScanner.hasNextInt();
      if (true){
        numOfRows = myScanner.nextInt();
        if (numOfRows >0 && numOfRows <=10){
          flag = false;
        }
      } else {
        String random = myScanner.next();
      }
    }
    for (int rows = 1; rows <= numOfRows; rows++){
      for (int columns = 1; columns <= numOfRows - rows + 1 ; columns++){
        System.out.println(columns + "");
      }
      System.out.println("");
    }
  }
}

