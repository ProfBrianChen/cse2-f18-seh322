////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Pattern B
//Lab 06
import java.util.Scanner; //must import the scanner
public class PatternB{
  public static void main(String[] args){ //state the main method
    Scanner myScanner = new Scanner(System.in);
    
    boolean flag = true; //use loops to see if the number will be true to the statement
    int numOfRows = 0; //declare the variable
    while (flag){ //use while loops as directed
      System.out.println("Enter a number from 1-10:"); //tell the use what to input
      boolean right = myScanner.hasNextInt();
      if (true){
        numOfRows = myScanner.nextInt();
        if (numOfRows >0 && numOfRows <=10){ //state the number that must be inputed to be between 1-10
          flag = false;
        }
      } else {
        String random = myScanner.next();
      }
    }
    for (int rows = 1; rows <= numOfRows; rows++){
      for (int columns = 1; columns <= numOfRows - rows + 1 ; columns++){
        System.out.print  (columns + " ");
      }
      System.out.println();
    }
  }
}