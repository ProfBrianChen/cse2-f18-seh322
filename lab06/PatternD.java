////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Pattern D
//Lab 06
import java.util.Scanner;
public class PatternD{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    //declare the boolean
    boolean flag = true;
    int numOfRows = 0;
    while (flag){
      System.out.println("Enter a number from 1-10:");
      boolean right = myScanner.hasNextInt();
      if (true){
        numOfRows = myScanner.nextInt();
        if (numOfRows >0 && numOfRows <=10){
          flag = false;
        }
      } else {
        String random = myScanner.next();
      }
    }
    for (int rows = 1; rows <= numOfRows; rows++){
      for (int columns = numOfRows - rows + 1; columns >= 1; columns--){
        System.out.print(columns + " ");
      }
      System.out.println();
    }
  }
}