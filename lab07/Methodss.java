////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Methods
import java.util.Scanner;
import java.util.Random; //import scanner
public class Methodss{
  public static String adjective(){
    Random randomGenerator = new Random();
    int random = (int) (randomGenerator.nextInt(10)); //this generates numbers less than 10
    String adjectiveString = "";
    switch (random){
      case 0: adjectiveString = "big";
        break;
      case 1: adjectiveString = "small";
        break;
      case 2: adjectiveString = "tall";
        break;
      case 3: adjectiveString = "short";
        break;
      case 4: adjectiveString = "fast";
        break;
      case 5: adjectiveString = "slow";
        break;
      case 6: adjectiveString = "clumsy";
        break;
      case 7: adjectiveString = "friendly";
        break;
      case 8: adjectiveString = "lazy";
        break;
      case 9: adjectiveString = "shy";
        break;
    }
    return adjectiveString;
  }
  public static String subject(){
    Random randomGenerator = new Random();
   int random = (int) (randomGenerator.nextInt(10)); //this generates numbers less than 10
    String subjectString = "";
    switch (random){
      case 0: subjectString = "cat";
        break;
      case 1: subjectString = "dog";
        break;
      case 2: subjectString = "cow";
        break;
      case 3: subjectString = "lion";
        break;
      case 4: subjectString = "pig";
        break;
      case 5: subjectString = "bird";
        break;
      case 6: subjectString = "fox";
        break;
      case 7: subjectString = "wolf";
        break;
      case 8: subjectString = "horse";
        break;
      case 9: subjectString = "fish";
        break;
    }
    return subjectString;
  }
  public static String verb(){
    Random randomGenerator = new Random();
    int random = (int) (randomGenerator.nextInt(10)); //this generates numbers less than 10
    String verbString = "";
    switch (random){
      case 0: verbString = "passed";
        break;
      case 1: verbString = "saw";
        break;
      case 2: verbString = "waved";
        break;
      case 3: verbString = "tested";
        break;
      case 4: verbString = "jumped";
        break;
      case 5: verbString = "slept";
        break;
      case 6: verbString = "ran";
        break;
      case 7: verbString = "walked";
        break;
      case 8: verbString = "stretched";
        break;
      case 9: verbString = "yawned";
        break;
    }
    return verbString;
  }
  public static String object(){
    Random randomGenerator = new Random();
   int random = (int) (randomGenerator.nextInt(10)); //this generates numbers less than 10
    String objectString = "";
    switch (random){
      case 0: objectString = "baby";
        break;
      case 1: objectString = "girl";
        break;
      case 2: objectString = "boy";
        break;
      case 3: objectString = "man";
        break;
      case 4: objectString = "woman";
        break;
      case 5: objectString = "teacher";
        break;
      case 6: objectString = "coach";
        break;
      case 7: objectString = "butterfly";
        break;
      case 8: objectString = "ladybug";
        break;
      case 9: objectString = "spider";
        break;
    }
    return objectString;
  }
  
  public static void main(String args[]){ //main method
   Scanner myScanner = new Scanner(System.in);
    boolean sentence = true;
    String answer = "";
    String subject = subject();
 do{
   System.out.print("Do you want a sentence? ('Yes' or 'No'):");
   answer = myScanner.nextLine();
   if (answer.equals("Yes")){
     System.out.println("The " + adjective() + " " + subject + " " + verb() + " " + "the" + " " + object());
   }
   else{
     break;
   }
 }while (answer.equals("Yes"));
    secondSentence(subject);
      }
   
  public static String secondSentence(String subject){
    
  }