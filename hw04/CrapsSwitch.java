////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 CrapsSwitch
import java.util.Scanner; //import scanner
public class CrapsSwitch{
  public static void main(String args[]){ //main method
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter 1 randomly generate dice");
    int option = myScanner.nextInt();
    int rollOne = 0;
    int rollTwo = 0;
    
    if (option == 1){ //this is option 1 meaning it is a random dice generator
    rollOne = (int) (Math.random() * 6) + 1; //dice roll one will be a number 1 through 6
    rollTwo = (int) (Math.random() * 6) + 1; //dice roll two will be a number 1 through 6
    System.out.println("You rolled a " + rollOne + " and a " + rollTwo + ".");
      
    int result = (rollOne + rollTwo);
    String outcome;
    String numberString;
    switch (result){
      case 2: result = 2;
        System.out.println("Snake Eyes");
        break;
      case 3: result = 3;
        System.out.println("Ace Deuce");
        break;
      case 4: result = 4;
        System.out.println("Easy Four");
        break;
      case 5: result = 5;
        System.out.println("Fever Five");
        break;
      case 6: result = 6;
        System.out.println("Easy Six");
        break;
      case 7: result = 7;
        System.out.println("Seven Out");
        break;
      case 8: result = 8;
        System.out.println("Easy Eight");
        break;
      case 9: result = 9;
        System.out.println("Nine");
        break;
      case 10: result = 10;
        System.out.println("Easy Ten");
        break;
      case 11: result = 11;
        System.out.println("Yo-leven");
        break;
      case 12: result = 12;
        System.out.println("Boxcars");
        break;
        
    }
      System.out.println("Your result is a " + result);
  }
  }
}