////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 CardGenerator
import java.lang.Math;
public class CardGenerator{
  public static void main(String args[]){
    System.out.println("Pick a random card from the deck ");
    int number = (int) (Math.random() * 52) + 1;
    int typeOfSuit = (int) (Math.random() * 52) + 1;
    String suit = "Null";
    if (number >0 && number <=13){ //using an if statement to be able to have a random number in a certain sequence be a suit
      //System.out.println("You have chosen a spade"); //if between 0-13 then will receive a spade
      suit = "Spades"; //we must state the suit of the card that will be chosen
    }
    else if (number >13 && number <=26){
      //System.out.println("You have chosen a heart"); //if between 14-26 then will receive a heart
      suit = "Hearts";
    }
    else if (number >26 && number <=39){
      //System.out.println("You have chosen a diamond"); //if between 27-39 then will receive a diamond
      suit = "Diamonds";
    }
    else if (number >39 && number <=52){
      //System.out.println("You have chosen a club"); //if between 40-52 then will receive a club
      suit = "Clubs";
    }
    int face = number;
    String faceString;
    switch (face) {
      case 1: faceString = "Ace";
        break;
      case 2: faceString = "2";
        break;
      case 3: faceString = "3";
        break;
      case 4: faceString = "4";
        break;
      case 5: faceString = "5";
        break;
      case 6: faceString = "6";
        break;
      case 7: faceString = "7";
        break;
      case 8: faceString = "8";
        break;
      case 9: faceString = "9";
        break;
      case 10: faceString = "10";
        break;
      case 11: faceString = "Jack";
        break;
      case 12: faceString = "Queen";
        break;
      case 13: faceString = "King";
        break;
      default: faceString = "Invalid face";
        break;
    }
   // System.out.println(faceString);
    System.out.println("You have chosen a " + faceString + " of " + suit);
  }
}
    
