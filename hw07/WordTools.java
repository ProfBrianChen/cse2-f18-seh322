////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Word Tools
import java.util.Scanner; //import scanner
public class WordTools{ //name of program
  public static Scanner myScanner = new Scanner(System.in); //set up scanner
  public static int getNumOfNonWSCharacters(String text){ //set up method for number of nonwhite space characters
    text = text.trim().replaceAll("\\s",""); //this will eliminate the white spaces and find length of remainding characters
    return text.length(); //return the length of characters to main method
  }
  public static int getNumOfWords(String text){ //method for number of words
    text = shortenSpace(text);
    String[] words = text.split(" ");
    return text.length(); //return the amount of words
  }
  public static int findText(String text, String find){ //method for finding word count
    int amount = 0;
    int i = 0;
    String word = "";
    do{
      System.out.println("Enter a word or phrase to be found: ");
      word = myScanner.nextLine();
      if (word.equals("")){
        System.out.println("" + " " + "instances:" + amount);
      }
    }while (word.equals(""));
    return amount;
  }
  public static String replaceExclamation(String text){ //method dor replacing exclamation
    String flag = text.replaceAll("!","."); //replace all !s with a "." instead
    return flag; //return value for inside main method
  }
  public static String shortenSpace(String text){
    String flag = text.trim().replaceAll("+","");
    return flag;
  }
  public static void printMenu(){ //set up the method for the menu
    System.out.println("MENU"); //print according to hw instructions
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Enter one of the letter options: ");
  }
  public static void main(String[] args){ //set up the main method for the code to run
    while(true){ //use a loop to go through the whole program
      System.out.println("Type in a sample text:"); //allow user to input their own text
      String text = myScanner.nextLine(); //use their input 
      System.out.println("You typed:" + text); //tell user what they typed
      printMenu();
      //print the options they can choose 
      char x = myScanner.nextLine().charAt(0); //use the option they choose to determine what to output
      switch (x){ //use switch statement to determine what function will be used
        case 'c': //state the different functions based on the hw instructions
          int countNonWSCharacters = getNumOfNonWSCharacters(text); //get the number of non-whitespace characters
          System.out.println("Number of non-whitespace characters: " + countNonWSCharacters); //print the line and the number of characters
          break; //break to stop switch statement
        case 'w':
          int countOfWords = getNumOfWords(text); //get number for the amount of words
          System.out.println("Number of words: " + countOfWords); //print the line of number of words
          break;
        case 'f': 
          System.out.println("Enter a word to be counted: "); //print which word tp be counted
          String find = myScanner.nextLine(); //let user input the word
          int countOfFindWords = findText(text, find); //count the number of times the word was entered
          System.out.println("Find text: " + countOfFindWords); //print the word
          break;
        case 'r':
          String symbol = replaceExclamation(text); //declare variable to replace the symbol
          System.out.println("New text: " + symbol);
          break;
        case 's':
          symbol = shortenSpace(text);
          System.out.println("New text: " + symbol);
          break;
        default:
          System.out.println("Wrong input. Please re-enter option"); //wrong key and allow user to try again
      }
    }
  }
}