////Sarah Hua
////Professor Arielle Carr
////Tuesdays and Thursdays
////CSE 02 Convert
///
   import java.util.Scanner;
public class Convert{
  public static void main(String args[]){
    //hurricane analysis and stats
    //main method required for every Java program
    Scanner myScanner = new Scanner (System.in);
    System.out.print("Type in a double to represent area of land affected by hurricane:");
    double acres = myScanner.nextDouble();
    System.out.print("Land affected by hurricane"+acres); //state the amount of land affected in acres
    System.out.print("Enter the average number of inches of rain: "); //to be able to state the average amount of rain
    double inchesofRain = myScanner.nextDouble();
    //43560 square feet in 1 acre
    double squareInchesPerAcre = 43560 * 12 * 12;
    double squareInchesAffected = squareInchesPerAcre * acres;
    double cubicAffected = squareInchesAffected * inchesofRain; //to find the amount of area affected
    //5280 feet per mile
    double inchesPerMile = 5280 * 12; //converting to get total number of inches
    double cubicInchesPerMile = inchesPerMile * inchesPerMile * inchesPerMile; //converting to cubic inches
    double ret = cubicAffected / cubicInchesPerMile; 
    System.out.print(ret);
    
  }
}

