////Sarah Hua 
////Professor Carr
////Tuesdays and Thursdays
////CSE 02 Pyramid
///
import java.util.Scanner;
public class Pyramid{
  public static void main(String args[]){
    //main method required for every Java program
    //finding the volume of a square pyramid
    Scanner myScanner = new Scanner (System.in);
    System.out.print("The square side of the pyramid is (input length):"); 
    double base = myScanner.nextDouble(); //given base for the pyramid
    System.out.print("The height of the pyramid is (input height):");
    double height = myScanner.nextDouble(); //given height for the pyramid
    double volume = base * base * height/3; //formula for a rectangular pyramid
    System.out.print("The volume inside the pyramid is:"+volume);
    }
    }
    
